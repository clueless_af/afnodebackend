const mongoose = require('../DBSchema/SchemaMapper')
const examModel = mongoose.model('exam')

const Exam = function () {

    this.add = (data) => {
        return new Promise((resolve, reject) => {
            const exam = new examModel(data)
            exam.save().then(() => {
                resolve({status: 200, message: "New Exam added"});
            }).catch(err => {
                reject({status: 500, message: err});
            })
        })
    }

    this.searchAll = () => {
        return new Promise((resolve, reject) => {
            examModel.find().exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }

    this.search = (id) => {
        return new Promise((resolve, reject) => {
            examModel.findOne({_id: id}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }

    this.update = (coursename, data) => {
        return new Promise((resolve ,reject) =>{
            examModel.update({coursename: coursename,data}).then( (result)=>{
                resolve({status: 200,data: result})
            }).catch( err =>{
                reject ({status: 500 , error: err})
            })
        })
    }

    this.delete = (id) => {
        return new Promise((resolve, reject) => {
            examModel.deleteOne({_id: id}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }
}

module.exports = new Exam()