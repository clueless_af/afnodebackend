const mongoose = require('../DBSchema/Schema')
const assignmentModel = mongoose.model('assignment')
const notifyModel = mongoose.model('notification')

const Assignment = function () {
    this.add = (data) => {
        return new Promise((resolve, reject) => {
            const crs = new assignmentModel(data);
            crs.save().then(() => {
                resolve({status: 200, message: "Assignment added"})
                const notify = new notifyModel(data) 
                notify.save().then( ()=>{
                    resolve({status: 200, message: "Notification added"})
                })
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }

    this.searchAll = () => {
        return new Promise((resolve, reject) => {
            assignmentModel.find().exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }

    this.search = (id) => {
        return new Promise((resolve, reject) => {
            assignmentModel.find({_id: id}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    };

    this.searchCourse = (id) => {
        return new Promise((resolve, reject) => {
            assignmentModel.find({submittedBy: id}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    };

    this.searchAss = (id) => {
        return new Promise((resolve, reject) => {
            assignmentModel.find({coursename: id}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    };




    this.getupdate = (name, data) => {
        return new Promise((resolve ,reject) =>{
            examModel.updateOne({name: name,data}).then( (result)=>{
                resolve({status: 200,data: result})
            }).catch( err =>{
                reject ({status: 500 , error: err})
            })
        })
    }

    this.delete = (id) => {
        return new Promise((resolve, reject) => {
            assignmentModel.deleteOne({_id: id}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }
}

module.exports = new Assignment()