const mongoose = require('../DBSchema/Schema')
const notificationModel = mongoose.model('notification')

const Notification = function () {
    this.add = (data) => {
        return new Promise((resolve, reject) => {
            const noti = new notificationModel(data)
            noti.save().then(() => {
                resolve({status: 200, message: "Notification added for assignment for students"})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }

    this.searchAll = () => {
        return new Promise((resolve, reject) => {
            notificationModel.find().exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }

    this.search = (coursename) => {
        return new Promise((resolve, reject) => {
            notificationModel.findById({coursename: coursename}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }

    this.getupdate = (name, data) => {
        return new Promise((resolve ,reject) =>{
            notificationModel.updateOne({name: name,data}).then( (result)=>{
                resolve({status: 200,data: result})
            }).catch( err =>{
                reject ({status: 500 , error: err})
            })
        })
    }

    this.delete = (id) => {
        return new Promise((resolve, reject) => {
            notificationModel.deleteOne({_id: id}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, error: err})
            })
        })

    }
}

module.exports = new Notification()