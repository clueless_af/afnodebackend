const express = require('express')
const router = express.Router()
const exam = require('./ExamController')

router.post('/', (req, res) => {
    exam.add(req.body).then(data => {
        res.status(data.status).send({msg: data.message});
    }).catch(err => {
        res.status(err.status).send({msg: err.message});
        console.log("error")
    })
});

router.get('/:id', (req, res) => {
    exam.search(req.params.id).then((data => {
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

router.get('/', (req, res) => {
    exam.searchAll().then((data => {
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

router.put('/:id' , (req,res)=> {
    exam.update(req.params.id).then( (data =>{
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

router.delete('/:id' , (req,res)=> {
    exam.delete(req.params.id).then( (data =>{
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

module.exports = router