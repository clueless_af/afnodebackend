const express = require('express')
const router = express.Router()
const notify = require('./NotificationController')

router.post('/', (req, res) => {
    notify.add(req.body).then((data => {
        res.status(data.status).send(data.message)
    })).catch(err => {
        res.status(err.status).send(err.message)
        console.log("error")
    })
})

router.get('/:coursename', (req, res) => {
    notify.search(req.params.coursename).then((data => {
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

router.get('/', (req, res) => {
    notify.searchAll().then((data => {
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

router.put('/:id' , (req,res)=> {
    notify.update(req.params.id).then( (data =>{
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

router.delete('/:id' , (req,res)=> {
    notify.delete(req.params.id).then( (data =>{
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

module.exports = router