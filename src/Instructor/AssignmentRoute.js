<<<<<<< HEAD
const express = require('express');
const router = express.Router();
const assignment = require('./AssigmentController');
=======
const express = require('express')
const router = express.Router()
const assignment = require('./AssignmentController')
>>>>>>> fe68d937422adb93aab036f0eabd8d05886f4b5e

router.post('/', (req, res) => {
    assignment.add(req.body).then((data => {
        res.status(data.status).send(data.message)
    })).catch(err => {
        res.status(err.status).send(err.message)
        console.log("error")
    })
})

router.get('/:id', (req, res) => {
    assignment.search(req.params.id).then((data => {
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
});

router.get('/course/:nic', (req, res) => {
    assignment.searchCourse(req.params.nic).then((data => {
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
});

router.get('/ass/:coursename', (req, res) => {
    assignment.searchAss(req.params.courses).then((data => {
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
});


router.get('/', (req, res) => {
    assignment.searchAll().then((data => {
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

router.put('/:id' , (req,res)=> {
    assignment.update(req.params.id).then( (data =>{
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

router.delete('/:id' , (req,res)=> {
    assignment.delete(req.params.id).then( (data =>{
        res.status(data.status).send(data.data)
    })).catch(err => {
        res.status(err.status).send(err.message)
    })
})

module.exports = router