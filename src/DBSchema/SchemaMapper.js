const mongoose = require('mongoose')
const Schema = mongoose.Schema

const subject = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    }
})

const exam = new Schema({
   
    coursename: {
        type: String,
        required: true
    },
    duration: {
        type: Number,
        required: true
    },
    exam_type: {
        type: String,
        required: true
    },
    due_dates: {
        type: Date, 
        required: true
    }

}) 

const assignment = new Schema({

    coursename: {
        type: String,
        required: true
    },
    assignmentname: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    due_dates: {
        type: Date, 
        required: true
    },
    submittedBy:{
        type: String,
        required:false
    }

})

const notification = new Schema({

    coursename: {
        type: String,
        required: true
    },
   
    description: {
        type: String,
        required: false
    },
    due_dates: {
        type: Date, 
        required: true
    }
})

const notificationIns = new Schema({

    courses: {
        type: [String],
        required: false
    },
<<<<<<< HEAD
    sname: {
=======
   
    nic: {
        type: String,
        required: false
    },
    
})

const user = new Schema({
  
    name: {
>>>>>>> fe68d937422adb93aab036f0eabd8d05886f4b5e
        type: String,
        required: false
    },
    nic: {
        type: String,
        required: false
    },
    birthday: {
<<<<<<< HEAD
        type: String,
        required: false
=======
        type: Date,
        required: true
>>>>>>> fe68d937422adb93aab036f0eabd8d05886f4b5e
    },
    email: {
        type: String,
        required: false
    },
    mobileNo: {
        type: Number,
        required: false
    },
    courses: {
        type: [String],
        required : false 
    },
    password: {
        type: String,
        required: false
    },
    userLevel:{
        type:Number,
        required: true
    }
})

const course = new Schema({
    cName: {
        type: String,
        required: true
    },
    cCode: {
        type: String,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    instructors: {
        type: Array,
        required: false
    }
})

mongoose.model('subject', subject)
mongoose.model('exam', exam)
mongoose.model('assignment', assignment)
mongoose.model('user', user)
mongoose.model('notification', notification)
mongoose.model('notificationIns', notificationIns)
mongoose.model('course', course)

mongoose.connect('mongodb://localhost/sliittest', {useNewUrlParser: true}, err => {
    if (err) {
        console.log(err)
        return
    }
    console.log('connected to "sliit" mongoDB database')
})

module.exports = mongoose