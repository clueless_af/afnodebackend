var express = require('express');
var router = express.Router();
var Controller = require('./Student.Controller');

router.post('/add', (req, res) => {
    Controller.insert(req.body).then(data => {
        res.status(data.status).send({msg: data.message});
    }).catch(err=>{
        console.log(err);
    })
});

router.put('/:id', (req, res) => {
    Controller.updateStudent(req.params.nic, req.body).then((data) => {
        res.status(data.status).send({message: data.msg});
    }).catch(err => {
        res.status(err.status).send({msg: err.message});
    })
});

router.get('/getAll', (req, res) => {
    Controller.getAllStudent().then(data => {
        res.status(data.status).send({data: data.data});
    }).catch(err => {
        res.status(err.status).send({message: data.message});
    })
});

router.get('/:id', (req, res) => {
    Controller.getStudent(req.params.nic).then(data => {
        res.status(data.status).send({data: data.data});
    }).catch(err => {
        res.status(err.status).send({message: data.msg});
    })
});


module.exports = router;
