var mongoose = require('../DBSchema/SchemaMapper');
var StudentSchema = mongoose.model('user');

var StudentController = function () {
//signUp student
    this.insert = (data) => {
        return new Promise((resolve, reject) => {
            var student = new StudentSchema({
                name: data.name,
                nic: data.nic,
                birthday: data.birthday,
                email: data.email,
                mobileNo: data.mobileNo,
                courses: data.courses,
                password: data.password,
                userLevel: 3
            });
      //      const student = new StudentSchema(data);
            student.save().then(() => {
                resolve({status: 200, message: "New student added"});
            }).catch(err => {
                reject({status: 500, message: err});
            });
        });
    }
//get student by id
//can use for login
    this.getStudent = (nic) => {
        return new Promise(((resolve, reject) => {
            StudentSchema.find({nic: nic}).exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, msg: err});
            })
        }))
    };
//get all students
    this.getAllStudent = () => {
        return new Promise(((resolve, reject) => {
            StudentSchema.find().exec().then((data) => {
                resolve({status: 200, data: data})
            }).catch(err => {
                reject({status: 500, message: err});
            })
        }))
    };

//update Student details
    this.updateStudent = (nic, data) => {
        return new Promise((resolve, reject) => {
            StudentSchema.update({nic: nic, data}).then(() => {
                resolve({status: 200, message: "success"});
            }).catch(err => {
                reject({status: 500, message: err});
            })
        })
    };
};

module.exports = new StudentController();